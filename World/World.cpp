#include "./World.h"

#define WORLD_SIZE 20

using namespace std;

void World::PrintBorder()
{
    clear();
    // Calculate starting coordinates to center the game window   
    int x_start = COLS / 2 - WORLD_SIZE - 2;  
    int y_start = LINES / 2 - WORLD_SIZE / 2;
    
    // Top decoration
    SetColor(' ');
    move(y_start - 1, x_start);
   
    printw(" Snek.exe"); 
    for (int i = 9; i < (WORLD_SIZE+2) * 2 - 6; i++) printw(" ");
    SetColor('X'); printw("XXX");
    SetColor('A'); printw("AAA");
    
    
    // Print borders
    // Top horisontal
    SetColor('#');
    move(y_start, x_start);
    for (int i = 0; i < (WORLD_SIZE+2) * 2; i++) {
        printw("#");
    }

    // Bottom horisontal
    move(y_start + WORLD_SIZE + 1, x_start);
    for (int i = 0; i < (WORLD_SIZE+2) * 2; i++) {
        printw("#");
    }

    // Sides
    for (int y = 1; y < WORLD_SIZE + 1; y++) {
        move(y_start + y, x_start);
        printw("##");
        move(y_start + y, x_start + WORLD_SIZE*2 + 2);
        printw("##");
    } 
    
    // Text
    SetColor(' ');
    move(y_start + WORLD_SIZE + 2, x_start);
    printw(" Score:%-3d Speed:%-3d     Bad Snek by Jaanus ", snake.size - 1, snake.speed);
}

void World::PrintWorld()
{ 
    // Reset world
    for (int y = 0; y < WORLD_SIZE; y++) {
        for (int x = 0; x < WORLD_SIZE; x++) {
            grid[x][y] = 'o';
        }
    }
    
    // Add apple
    grid[apple.x][apple.y] = 'A';
    
    // Add snake
    AddSnake();

    // Print world 
    int x_start = COLS / 2 - WORLD_SIZE;
    int y_start = (LINES - WORLD_SIZE) / 2  + 1;
    move(y_start, x_start);
    for (int y = 0; y < WORLD_SIZE; y++) {
        for (int x = 0; x < WORLD_SIZE; x++) {
            // Set color
            SetColor(grid[x][y]); 
            printw("%c ", grid[x][y]);
        } 
        move(y_start + 1 + y, x_start);
    }
    
    // Render drawing
    refresh();
}

void World::PrintEnd()
{
    char game_over[] = "Game Over!";
    char score_text[] = "Score: 000";
    char quit_text[] = " Press 'q' to quit ";
    sprintf(score_text, "Score: %03d", snake.size - 1);    

    SetColor('t');
    move(LINES / 2 - 1, COLS / 2 - strlen(game_over) / 2);
    printw("%s", game_over);
    
    move(LINES / 2, COLS / 2 - strlen(score_text) / 2);
    printw("%s", score_text); 

    move(LINES / 2 + 1, COLS / 2 - strlen(quit_text) / 2);
    printw("%s", quit_text);

    refresh();
}

void World::AddSnake()
{
   char heads[4] = { 'v', '>', '^', '<' };
   grid[snake.x[snake.size - 1]][snake.y[snake.size - 1]] = heads[snake.direction - 1]; 
    for (int cell = 0; cell < snake.size - 1; cell++)
    { 
        int x = snake.x[cell];
        int y = snake.y[cell]; 
        grid[x][y] = 'X';
    }
}

void World::MoveSnake()
{    
   // Move the ghost cell
   snake.ghost_x = snake.x[0];
   snake.ghost_y = snake.y[0];

   // Shift all snake cells by 1 square
   for (int cell = 0; cell < snake.size - 1; cell++)
   {
        snake.x[cell] = snake.x[cell + 1];
        snake.y[cell] = snake.y[cell + 1];
   } 

    // Set new head location
    int snake_size = snake.size;
    switch (snake.direction) {
        case 1: // North
            snake.y[snake_size - 1] -= 1;
            break;
        case 2: // West
            snake.x[snake_size - 1] -= 1;
            break; 
        case 3: // South
            snake.y[snake_size - 1] += 1;
            break;
        case 4: // East
            snake.x[snake_size - 1] += 1;
            break;
    }

   CheckEat(); 
}

void World::GrowSnake()
{
	// Shift cell memory locations to make room for new piece
	for (int cell = snake.size; cell >= 0; cell--)
	{
		snake.x[cell] = snake.x[cell - 1];
        snake.y[cell] = snake.y[cell - 1];
	}

	// Increment size
	snake.size++;

	// Add tail
	snake.x[0] = snake.ghost_x;
	snake.y[0] = snake.ghost_y;

    // Increase game speed 
    if (snake.speed >= 40) snake.speed -= sqrt(3 * snake.speed / 2);//snake.speed -= 40; 
}

bool World::CheckCollision()
{
    // Check self intersection
    int head = snake.size - 1;
    
    for (int cell = head - 1; cell >= 0; cell--) {
        if ( (snake.x[head] == snake.x[cell]) && (snake.y[head] == snake.y[cell]) ) {
            return true;     
        }
    }

    // Check out of bounds
    if ((snake.x[snake.size - 1] < 0) || (snake.x[snake.size - 1] >= WORLD_SIZE)) return true;
    if ((snake.y[snake.size - 1] < 0) || (snake.y[snake.size - 1] >= WORLD_SIZE)) return true;

    return false;

}

void World::SpawnApple()
{
    int x, y;
    
    // Search for a spot with no snake
    while(1)
    {
        x = rand() % WORLD_SIZE;
        y = rand() % WORLD_SIZE;

        for (int cell = 0; cell < snake.size; cell++)
        {
            if ((snake.x[cell] == x) && (snake.y[cell] == y))
            {
                goto search_again; // goto is useful, change my mind
            }
        }
        break; // No snake in that spot, break while(1)
        search_again:
        continue; // Snake in that spot, retry while(1)
    }

    apple.x = x;
    apple.y = y; 
}

void World::CheckEat()
{ 
    if ((snake.x[snake.size - 1] == apple.x) && (snake.y[snake.size - 1] == apple.y))
    {
        GrowSnake();
        SpawnApple();
    }
}

void World::SetColor(char tile) {
    switch (tile) {
        case 'o':
             attron(COLOR_PAIR(1));
             break;
        case 'X':
             attron(COLOR_PAIR(2));
             break;
        case 'A':
             attron(COLOR_PAIR(4));
             break;
        case '<':
            attron(COLOR_PAIR(3));
            break;
        case '>':
            attron(COLOR_PAIR(3));
            break;
        case '^':
            attron(COLOR_PAIR(3)); 
            break;
        case 'v':
            attron(COLOR_PAIR(3));
            break;
        case '#':
            attron(COLOR_PAIR(5));
            break;
        default:
             attron(COLOR_PAIR(6));
             break;
    }
}
